<?xml version="1.0" encoding="UTF-8"?>

<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<UiMod name="DPSMeter" version="1.0.1a" date="3/3/2011" >

		<Author name="Varonth" email="" />
		
		<VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.1" />

		<Description text="DPSMeter" />

		<Dependencies>
			<Dependency name="LibSlash" optional="true" />
			<Dependency name="LibAddonButton" optional="true" />
		</Dependencies>

		<Files>
			<File name="DPSMeter.lua" />
			<File name="DPSMeterWindow.xml" />
			<File name="DPSMeterWindow.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="DPSMeter.Initialize" />
		</OnInitialize>

		<SavedVariables>
			<SavedVariable name="DPSMeter.Stats" />
		</SavedVariables>

	</UiMod>

</ModuleFile>