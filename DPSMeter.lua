local pairs, ipairs = pairs, ipairs

local DPSTable = nil
local CEvents = GameData.CombatEvent

local combat = 0
local timeeventregistered = false
local timepassed = 0

local NoCareerAbilities = {
	14435, -- Oil
	24684, -- RAMS
	24694, -- SingleTarget Cannon
	24682  -- AoE Cannon
}

DPSMeter = {}

local dps = DPSMeter

dps.Stats = {}

function dps.Initialize()
	if dps.Stats[GameData.Player.name] == nil then
		dps.Stats[GameData.Player.name] = {}
		dps.Stats[GameData.Player.name].meta = {}
		dps.Stats[GameData.Player.name].meta.time = 0
	end
	DPSTable = dps.Stats[GameData.Player.name]
	if not GameData.Player.isPlayAsMonster then
		RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "DPSMeter.OnCombatEvent")
	end
	RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "DPSMeter.OnCombatFlagUpdated")
	RegisterEventHandler(SystemData.Events.PLAY_AS_MONSTER_STATUS, "DPSMeter.OnSkavenStatusChange")
	if LibSlash then
		LibSlash.RegisterSlashCmd("dpsmeter",DPSMeter.ShowWindow)
		if not LibSlash.IsSlashCmdRegistered("dps") then
			LibSlash.RegisterSlashCmd("dps",DPSMeter.ShowWindow)
		end
	end
	if LibAddonButton then
		LibAddonButton.Register("DPSMeter")
		LibAddonButton.AddMenuItem("DPSMeter",L"Show Window", DPSMeter.ShowWindow)
	end
	CreateWindow("DPSMeterWindow",false)
end

function dps.OnSkavenStatusChange(isSkaven)
	if not isSkaven then
		RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "DPSMeter.OnCombatEvent")
		RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "DPSMeter.OnCombatFlagUpdated")
		if GameData.Player.inCombat then
			RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "DPSMeter.OnUpdate")
		end
	else
		UnregisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "DPSMeter.OnCombatEvent")
		if timeeventregistered then
			UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "DPSMeter.OnUpdate")
		end
		UnregisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "DPSMeter.OnCombatFlagUpdated")
	end
end

-- Ram is ability ID - 24684

function dps.OnCombatEvent(worldObjID, value, combatEventType, abilityID)
	local cet = combatEventType
	d(worldObjID)
	d(value)
	d(combatEventType)
	d(abilityID)
	if worldObjID == GameData.Player.worldObjNum or worldObjID == GameData.Player.Pet.objNum then
		--TODO possible add of parsing how much damage each ability did to the player
		return
	end
	if cet == CEvents.IMMUNE then
		return
	end
	for i,v in ipairs(NoCareerAbilities) do
		if abilityID == v then
			return
		end
	end
	if cet == CEvents.BLOCK or cet == CEvents.PARRY or cet == CEvents.DISRUPT or cet == CEvents.DODGE or cet == CEvents.ABSORB then
		d("here")
		value = 0
	end
	local heal = 0
	local dmg = 0
	if value < 0 then
		dmg = value * (-1)
	elseif value > 0 then
		heal = value
	end
	if DPSTable[abilityID] == nil then
		DPSTable[abilityID] = {}
		DPSTable[abilityID].hits = 0
		DPSTable[abilityID].heals = 0
		
		DPSTable[abilityID].maxheal = 0
		DPSTable[abilityID].minheal = heal
		DPSTable[abilityID].maxdmg = 0
		DPSTable[abilityID].mindmg = dmg
		DPSTable[abilityID].dmg = 0
		DPSTable[abilityID].heal = 0
		
		DPSTable[abilityID].crits = 0
		
		DPSTable[abilityID].block = 0
		DPSTable[abilityID].dodge = 0
		DPSTable[abilityID].disrupt = 0
		DPSTable[abilityID].parry = 0
		
		DPSTable[abilityID].absorb = 0
		dps.ComboBoxNeedUpdate = true
	end
	
	if cet == CEvents.CRITICAL or cet == CEvents.ABILITY_CRITICAL then
		DPSTable[abilityID].crits = DPSTable[abilityID].crits + 1
	elseif cet == CEvents.BLOCK then
		DPSTable[abilityID].block = DPSTable[abilityID].block + 1
		heal = 0
		dmg = 0
	elseif cet == CEvents.PARRY then
		DPSTable[abilityID].parry = DPSTable[abilityID].parry + 1
		heal = 0
		dmg = 0
	elseif cet == CEvents.ABSORB then
		DPSTable[abilityID].absorb = DPSTable[abilityID].absorb + 1
		heal = 0
		dmg = 0
	elseif cet == CEvents.EVADE then
		DPSTable[abilityID].dodge = DPSTable[abilityID].dodge + 1
		heal = 0
		dmg = 0
	elseif cet == CEvents.DISRUPT then
		DPSTable[abilityID].disrupt = DPSTable[abilityID].disrupt + 1
		heal = 0
		dmg = 0
	end
	
	if dmg ~= 0 then
		DPSTable[abilityID].hits = DPSTable[abilityID].hits + 1
	end
	if heal ~= 0 then
		DPSTable[abilityID].heals = DPSTable[abilityID].heals + 1
	end
	
	if DPSTable[abilityID].maxheal < heal then
		DPSTable[abilityID].maxheal = heal
	end
	if DPSTable[abilityID].minheal > heal and heal > 0 then
		DPSTable[abilityID].minheal = heal
	end
	
	if DPSTable[abilityID].maxdmg < dmg then
		DPSTable[abilityID].maxdmg = dmg
	end
	if DPSTable[abilityID].mindmg > dmg and dmg > 0 then
		DPSTable[abilityID].mindmg = dmg
	end
	
	DPSTable[abilityID].dmg = DPSTable[abilityID].dmg + dmg
	DPSTable[abilityID].heal = DPSTable[abilityID].heal + heal
end

function dps.OnCombatFlagUpdated()
	if combat == 1 then
		if GameData.Player.inCombat then
			RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "DPSMeter.OnUpdate")
			timeeventregistered = true
		end
	end
	
	if combat == 2 then
		if not GameData.Player.inCombat then
			UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "DPSMeter.OnUpdate")
			timeeventregistered = false
			combat = 0
		end
	end
	if GameData.Player.inCombat then
		combat = combat + 1
	end
end

function dps.OnUpdate(elapsed)
	timepassed = timepassed + elapsed
	if timepassed > 1 then
		timepassed = timepassed - 1
		DPSTable.meta.time = DPSTable.meta.time + 1
	end
end

function dps.Reset()
	DPSTable = nil
	dps.Stats[GameData.Player.name] = nil
	dps.Stats[GameData.Player.name] = {}
	dps.Stats[GameData.Player.name].meta = {}
	dps.Stats[GameData.Player.name].meta.time = 0
	DPSTable = dps.Stats[GameData.Player.name]
	dps.ComboBoxNeedUpdate = true
end

function dps.FReset()
	dps.Stats = nil
	dps.ComboBoxNeedUpdate = true
end