local Window = "DPSMeterWindow"
local OverallTab = Window.."OverallTab"
local AbilityTab = Window.."AbilityTab"
local OverviewTab = Window.."DamageOverviewTab"
local timepassed = 0
local PosToId = {}
local comboboxID = 0
local NameToIds = {}
local sortmode = 1
local valuecombobox = nil
local pairs , ipairs = pairs, ipairs

DPSMeter.DamageOverview = {}

DPSMeter.ComboBoxNeedUpdate = true

local function CompareWStrings(a,b)
	for i=1,wstring.len(a) do
		local av = wstring.byte(a,i)
		local bv = wstring.byte(b,i)
		if av < bv then
			return true
		elseif av > bv then
			return false
		end
		if wstring.byte(b,i+1) == nil then
			return false
		end
	end
	return true
end

local function NCompareWStrings(a,b)
	return not CompareWStrings(a,b)
end

local function SortValues(a,b)
	return a.data > b.data
end

local function NSortValues(a,b)
	return not SortValues(a,b)
end

local function ListRefresh()
	local DPSTable = DPSMeter.Stats[GameData.Player.name]
	DPSMeter.DamageOverview = {}
	local t = {}
	for k,_ in pairs (NameToIds) do
		table.insert(t,k)
	end
	if sortmode == 1 then
		table.sort(t,CompareWStrings)
	elseif sortmode == 2 then
		table.sort(t,NCompareWStrings)
	end
	for k,v in ipairs(t) do
		local save = {}
		save.name = v
		local value = 0
		if valuecombobox == 1 then --dps
			local dmg = 0
			for i,j in pairs(NameToIds[v]) do
				dmg = dmg + DPSTable[j].dmg
			end
			value = dmg / DPSTable.meta.time
		elseif valuecombobox == 2 then -- hps
			local heal = 0
			for i,j in pairs(NameToIds[v]) do
				heal = heal + DPSTable[j].heal
			end
			value = heal / DPSTable.meta.time
		elseif valuecombobox == 3 then -- dmg
			for i,j in pairs(NameToIds[v]) do
				value = value + DPSTable[j].dmg
			end
		elseif valuecombobox == 4 then -- heal
			for i,j in pairs(NameToIds[v]) do
				value = value + DPSTable[j].heal
			end
		elseif valuecombobox == 5 then -- min dmg
			value = 99999999
			for i,j in pairs(NameToIds[v]) do
				if value > DPSTable[j].mindmg then
					value = DPSTable[j].mindmg
				end
			end
		elseif valuecombobox == 6 then -- min heal
			value = 99999999
			for i,j in pairs(NameToIds[v]) do
				if value > DPSTable[j].minheal then
					value = DPSTable[j].minheal
				end
			end
		elseif valuecombobox == 7 then -- max dmg
			value = 0
			for i,j in pairs(NameToIds[v]) do
				if value < DPSTable[j].maxdmg then
					value = DPSTable[j].maxdmg
				end
			end
		elseif valuecombobox == 8 then -- max heal
			value = 0
			for i,j in pairs(NameToIds[v]) do
				if value < DPSTable[j].maxheal then
					value = DPSTable[j].maxheal
				end
			end
		elseif valuecombobox == 9 then -- avg dmg
			local dmg = 0
			local hits = 0
			for i,j in pairs(NameToIds[v]) do
				dmg = dmg + DPSTable[j].dmg
				hits = hits + DPSTable[j].hits
			end
			if hits ~= 0 then
				value = dmg/hits
			else
				value = 0
			end
		elseif valuecombobox == 10 then -- avg heal
			local heal = 0
			local heals = 0
			for i,j in pairs(NameToIds[v]) do
				heal = heal + DPSTable[j].heal
				heals = heals + DPSTable[j].heals
			end
			if heals ~= 0 then
				value = heal/heals
			else
				value = 0
			end
		end
		save.data = value
		table.insert(DPSMeter.DamageOverview,save)
	end
	if sortmode == 3 then
		table.sort(DPSMeter.DamageOverview, SortValues)
	elseif sortmode == 4 then
		table.sort(DPSMeter.DamageOverview, NSortValues)
	end
	local t = {}
	for i=1,#DPSMeter.DamageOverview do
			table.insert(t, i)
	end
	ListBoxSetDisplayOrder(OverviewTab.."List", t)
end

local function UpdateOverallTab()
	local dmg = 0
	local heal = 0
	local hits = 0
	local heals = 0
	local maxheal = 0
	local minheal = 999999999
	local maxdmg = 0
	local mindmg = 999999999
	local crits = 0
	local block = 0
	local parry = 0
	local disrupt = 0
	local dodge = 0
	local absorb = 0
	local dps
	local hps
	local DPSTable = DPSMeter.Stats[GameData.Player.name]
	for k,v in pairs(DPSTable) do
		if k ~= "meta" then
			dmg = dmg + v.dmg
			heal = heal + v.heal
			absorb = absorb + v.absorb
			dodge = dodge + v.dodge
			disrupt = disrupt + v.disrupt
			parry = parry + v.parry
			block = block + v.block
			crits = crits + v.crits
			hits = hits + v.hits
			heals = heals + v.heals
			
			if maxdmg < v.maxdmg  then
				maxdmg = v.maxdmg
			end
			if mindmg > v.mindmg then
				mindmg = v.mindmg
			end
			
			if maxheal < v.maxheal  then
				maxheal = v.maxheal
			end
			if minheal > v.minheal then
				minheal = v.minheal
			end
		end
	end
	dps = dmg/DPSTable.meta.time
	hps = heal/DPSTable.meta.time
	local avgdmg = dmg/hits
	local avgheal = heal/heals
	
	if hits ~= 0 then
		critp = crits / hits * 100 -- in percent
	else
		critp = 0
	end
	
	if heals ~= 0 then
		healp = crits / heals * 100 -- in percent
	else
		healp = 0
	end
	local all = hits + block + parry + disrupt + dodge + absorb
	local blockp = block/all * 100
	local parryp = parry/all * 100
	local disruptp = disrupt/all * 100
	local dodgep = dodge/all * 100
	local absorbp = absorb/all * 100
	-- LabelSetText(OverallTab.."" , towstring(""..))
	LabelSetText(OverallTab.."DPSLabel" , towstring("DPS: "..string.sub(dps,string.find(dps,"%d+.?%d?%d?%d?"))))
	LabelSetText(OverallTab.."HPSLabel" , towstring("HPS: "..string.sub(hps,string.find(dps,"%d+.?%d?%d?%d?"))))
	LabelSetText(OverallTab.."DamageLabel" , towstring("Overall damage: "..dmg))
	LabelSetText(OverallTab.."HealLabel" , towstring("Overall heal: "..heal))
	LabelSetText(OverallTab.."MinDmgLabel" , towstring("Minimum damage: "..mindmg))
	LabelSetText(OverallTab.."MaxDmgLabel" , towstring("Maximum damage: "..maxdmg))
	LabelSetText(OverallTab.."MinHealLabel" , towstring("Minimum healing: "..minheal))
	LabelSetText(OverallTab.."MaxHealLabel" , towstring("Maximum healing: "..maxheal))
	
	LabelSetText(OverallTab.."BlockLabel" , towstring("Blocked: "..block))
	LabelSetText(OverallTab.."BlockPercentLabel" , towstring(string.sub(blockp,string.find(blockp,"%d+.?%d?%d?")).."% blocked"))
	LabelSetText(OverallTab.."ParryLabel" , towstring("Parried: "..parry))
	LabelSetText(OverallTab.."ParryPercentLabel" , towstring(string.sub(parryp,string.find(parryp,"%d+.?%d?%d?")).."% parried"))
	LabelSetText(OverallTab.."DisruptLabel" , towstring("Disrupted: "..disrupt))
	LabelSetText(OverallTab.."DisruptPercentLabel" , towstring(string.sub(disrupt,string.find(disrupt,"%d+.?%d?%d?")).."% disrupted"))
	LabelSetText(OverallTab.."DodgeLabel" , towstring("Dodged: "..dodge))
	LabelSetText(OverallTab.."DodgePercentLabel" , towstring(string.sub(dodgep,string.find(dodgep,"%d+.?%d?%d?")).."% dodge"))
	LabelSetText(OverallTab.."AbsorbLabel" , towstring("Absorbed: "..absorb))
	LabelSetText(OverallTab.."AbsorbPercentLabel" , towstring(string.sub(absorbp,string.find(absorbp,"%d+.?%d?%d?")).."% absorbed"))
	LabelSetText(OverallTab.."CritLabel" , towstring("Crits: "..crits))
	LabelSetText(OverallTab.."HitsLabel" , towstring("Hits: "..hits))
	LabelSetText(OverallTab.."HealsLabel" , towstring("Heals: "..heals))
	LabelSetText(OverallTab.."CritHitsLabel" , towstring("Crit percent: "..string.sub(critp, string.find(critp,"%d+.?%d?")).."%"))
	LabelSetText(OverallTab.."CritHealsLabel" , towstring("Healcrit percent: "..string.sub(healp, string.find(healp,"%d+.?%d?")).."%"))
	LabelSetText(OverallTab.."AvgDmgLabel" , towstring("Average damage per hit: "..string.sub(avgdmg,string.find(avgdmg,"%d+.?%d?%d?%d?"))))
	LabelSetText(OverallTab.."AvgHealLabel" , towstring("Average heal per heal: "..string.sub(avgheal,string.find(avgheal,"%d+.?%d?%d?%d?"))))
end

local function UpdateAbilityTab()
	local DPSTable = DPSMeter.Stats[GameData.Player.name]
	if DPSMeter.ComboBoxNeedUpdate then
		DPSMeter.ComboBoxNeedUpdate = false
		NameToIds = {}
		ComboBoxClearMenuItems(AbilityTab.."ComboBox")
		for k,_ in pairs(DPSMeter.Stats[GameData.Player.name]) do
			if k ~= "meta" and k ~= 0 then
				local name = GetAbilityName(k)
				if NameToIds[name] == nil then
					NameToIds[name] = {}
					table.insert(NameToIds[name], k)
				else
					table.insert(NameToIds[name], k)
				end
			elseif k == 0 then
				NameToIds[L"Auto Attack"] = {0}
			end
		end
		local t = {}
		for k,_ in pairs (NameToIds) do
			table.insert(t,k)
		end
		table.sort(t,CompareWStrings)
		for k,v in ipairs(t) do
			ComboBoxAddMenuItem(AbilityTab.."ComboBox",v)
		end
	end
	if comboboxID ~= 0 then
		local ids = NameToIds[ComboBoxGetSelectedText(AbilityTab.."ComboBox")]
		local stats = {hits = 0, heals = 0, maxheal = 0, minheal = 9999999, maxdmg = 0, mindmg = 999999, dmg = 0, heal = 0, crits = 0, block = 0, dodge = 0, disrupt = 0, parry = 0, absorb = 0}
		for k,v in pairs(ids) do
			local s = DPSTable[v]
			stats.hits = stats.hits + s.hits
			stats.heals = stats.heals + s.heals
			if stats.maxheal < s.maxheal then
				stats.maxheal = s.maxheal
			end
			if stats.minheal > s.minheal then
				stats.minheal = s.minheal
			end
			if stats.maxdmg < s.maxdmg then
				stats.maxdmg = s.maxdmg
			end
			if stats.mindmg > s.mindmg then
				stats.mindmg = s.mindmg
			end
			stats.dmg = stats.dmg + s.dmg
			stats.heal = stats.heal + s.heal
			stats.crits = stats.crits + s.crits
			stats.block = stats.block + s.block
			stats.dodge = stats.dodge + s.dodge
			stats.disrupt = stats.disrupt + s.disrupt
			stats.parry = stats.parry + s.parry
			stats.absorb = stats.absorb + s.absorb
		end	
		local dps = stats.dmg / DPSTable.meta.time 
		local hps = stats.heal / DPSTable.meta.time
		local avgdmg = stats.dmg/stats.hits
		if stats.hits == 0 then
			avgdmg = 0
		end
		local avgheal = stats.heal/stats.heals
		if stats.heals == 0 then
			avgheal = 0
		end
		local critp
		local healp
		
		if stats.hits ~= 0 then
			critp = stats.crits / stats.hits * 100 -- in percent
		else
			critp = 0
		end
	
		if stats.heals ~= 0 then
			healp = stats.crits / stats.heals * 100 -- in percent
		else
			healp = 0
		end
		
		local all = stats.hits + stats.block + stats.parry + stats.disrupt + stats.dodge + stats.absorb
		local blockp = 0
		local parryp = 0
		local disruptp = 0
		local dodgep = 0
		local absorbp = 0
		if all ~= 0 then
			blockp = stats.block/all * 100
			parryp = stats.parry/all * 100
			disruptp = stats.disrupt/all * 100
			dodgep = stats.dodge/all * 100
			absorbp = stats.absorb/all * 100
		end
		
		LabelSetText(AbilityTab.."DPSLabel" , towstring("DPS: "..string.sub(dps,string.find(dps,"%d+.?%d?%d?%d?"))))
		LabelSetText(AbilityTab.."HPSLabel" , towstring("HPS: "..string.sub(hps,string.find(hps,"%d+.?%d?%d?%d?"))))
		LabelSetText(AbilityTab.."DamageLabel" , towstring("Overall damage: "..stats.dmg))
		LabelSetText(AbilityTab.."HealLabel" , towstring("Overall heal: "..stats.heal))
		LabelSetText(AbilityTab.."MinDmgLabel" , towstring("Minimum damage: "..stats.mindmg))
		LabelSetText(AbilityTab.."MaxDmgLabel" , towstring("Maximum damage: "..stats.maxdmg))
		LabelSetText(AbilityTab.."MinHealLabel" , towstring("Minimum healing: "..stats.minheal))
		LabelSetText(AbilityTab.."MaxHealLabel" , towstring("Maximum healing: "..stats.maxheal))
	
		LabelSetText(AbilityTab.."BlockLabel" , towstring("Blocked: "..stats.block))
		LabelSetText(AbilityTab.."ParryLabel" , towstring("Parried: "..stats.parry))
		LabelSetText(AbilityTab.."DisruptLabel" , towstring("Disrupted: "..stats.disrupt))
		LabelSetText(AbilityTab.."DodgeLabel" , towstring("Dodged: "..stats.dodge))
		LabelSetText(AbilityTab.."AbsorbLabel" , towstring("Absorbed: "..stats.absorb))
		LabelSetText(AbilityTab.."CritLabel" , towstring("Crits: "..stats.crits))
		LabelSetText(AbilityTab.."HitsLabel" , towstring("Hits: "..stats.hits))
		LabelSetText(AbilityTab.."HealsLabel" , towstring("Heals: "..stats.heals))
		LabelSetText(AbilityTab.."CritHitsLabel" , towstring("Crit percent: "..string.sub(critp, string.find(critp,"%d+.?%d?")).."%"))
		LabelSetText(AbilityTab.."CritHealsLabel" , towstring("Healcrit percent: "..string.sub(healp, string.find(healp,"%d+.?%d?")).."%"))
		LabelSetText(AbilityTab.."AvgDmgLabel" , towstring("Average damage per hit: "..string.sub(avgdmg,string.find(avgdmg,"%d+.?%d?%d?%d?"))))
		LabelSetText(AbilityTab.."AvgHealLabel" , towstring("Average heal per heal: "..string.sub(avgheal,string.find(avgheal,"%d+.?%d?%d?%d?"))))
		LabelSetText(AbilityTab.."BlockPercentLabel" , towstring(string.sub(blockp,string.find(blockp,"%d+.?%d?%d?")).."% blocked"))
		LabelSetText(AbilityTab.."ParryPercentLabel" , towstring(string.sub(parryp,string.find(parryp,"%d+.?%d?%d?")).."% parried"))
		LabelSetText(AbilityTab.."DisruptPercentLabel" , towstring(string.sub(disruptp,string.find(disruptp,"%d+.?%d?%d?")).."% disrupted"))
		LabelSetText(AbilityTab.."DodgePercentLabel" , towstring(string.sub(dodgep,string.find(dodgep,"%d+.?%d?%d?")).."% dodge"))
		LabelSetText(AbilityTab.."AbsorbPercentLabel" , towstring(string.sub(absorbp,string.find(absorbp,"%d+.?%d?%d?")).."% absorbed"))
	else
		LabelSetText(AbilityTab.."DPSLabel" , L"")
		LabelSetText(AbilityTab.."HPSLabel" , L"")
		LabelSetText(AbilityTab.."DamageLabel" , L"")
		LabelSetText(AbilityTab.."HealLabel" , L"")
		LabelSetText(AbilityTab.."MinDmgLabel" , L"")
		LabelSetText(AbilityTab.."MaxDmgLabel" , L"")
		LabelSetText(AbilityTab.."MinHealLabel" , L"")
		LabelSetText(AbilityTab.."MaxHealLabel" , L"")
	
		LabelSetText(AbilityTab.."BlockLabel" , L"")
		LabelSetText(AbilityTab.."ParryLabel" , L"")
		LabelSetText(AbilityTab.."DisruptLabel" , L"")
		LabelSetText(AbilityTab.."DodgeLabel" , L"")
		LabelSetText(AbilityTab.."AbsorbLabel" , L"")
		LabelSetText(AbilityTab.."CritLabel" , L"")
		LabelSetText(AbilityTab.."HitsLabel" , L"")
		LabelSetText(AbilityTab.."HealsLabel" , L"")
		LabelSetText(AbilityTab.."CritHitsLabel" , L"")
		LabelSetText(AbilityTab.."CritHealsLabel" , L"")
		LabelSetText(AbilityTab.."AvgDmgLabel" , L"")
		LabelSetText(AbilityTab.."AvgHealLabel" , L"")
		LabelSetText(AbilityTab.."BlockPercentLabel" , L"")
		LabelSetText(AbilityTab.."ParryPercentLabel" , L"")
		LabelSetText(AbilityTab.."DisruptPercentLabel" , L"")
		LabelSetText(AbilityTab.."DodgePercentLabel" , L"")
		LabelSetText(AbilityTab.."AbsorbPercentLabel" , L"")
	end
end

local function Update()
	if WindowGetShowing(OverallTab) then
		UpdateOverallTab()
	elseif WindowGetShowing(AbilityTab) then
		UpdateAbilityTab()
	elseif WindowGetShowing(OverviewTab) then
		ListRefresh()
	end
end

function DPSMeter.InitializeWindow()
	LabelSetText(Window.."TitleBarText" , L"DPS Meter")
	ButtonSetText(Window.."TabsOverallTabButton" , L"Overall")
	ButtonSetText(Window.."TabsAbilityTabButton" , L"Abilities")
	ButtonSetText(OverallTab.."ResetButton", L"Reset")
	ButtonSetText(AbilityTab.."ToggleButton", L"Overview")
	ButtonSetText(OverviewTab.."ToggleButton", L"Abilties")
	LabelSetText(OverviewTab.."Name",L"Ability")
	LabelSetText(OverviewTab.."Value",L"Value")
	LabelSetTextColor(OverviewTab.."Name", DefaultColor.GOLD.r,DefaultColor.GOLD.g,DefaultColor.GOLD.b)
	LabelSetTextColor(OverviewTab.."Value", DefaultColor.WHITE.r,DefaultColor.WHITE.g,DefaultColor.WHITE.b)
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"DPS")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"HPS")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"Damage")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"Heal")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"Min. Damage")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"Min. Heal")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"Max. Damage")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"Max. Heal")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"Avg. Damage")
	ComboBoxAddMenuItem(OverviewTab.."ComboBox",L"Avg. Heal")
	ComboBoxSetSelectedMenuItem(OverviewTab.."ComboBox", 1)
	valuecombobox = 1
	for i=1,19 do
		if (i%2) == 0 then
			WindowSetAlpha(OverviewTab.."ListRow"..i.."Background", 0.4)
			WindowSetTintColor(OverviewTab.."ListRow"..i.."Background", DefaultColor.MEDIUM_LIGHT_GRAY.r,DefaultColor.MEDIUM_LIGHT_GRAY.g,DefaultColor.MEDIUM_LIGHT_GRAY.b)
		end
	end
end

function DPSMeter.OnClose()
	WindowSetShowing(Window,false)
end

function DPSMeter.OnHidden()
	UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "DPSMeter.WindowUpdate")
	WindowUtils.OnHidden()
end

function DPSMeter.OnShown()
	DPSMeter.ChangeTab(1)
	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "DPSMeter.WindowUpdate")
	WindowUtils.OnShown(DPSMeter.OnHidden, WindowUtils.Cascade.MODE_NONE)
end

function DPSMeter.WindowUpdate(elapsed)
	timepassed = timepassed + elapsed
	if timepassed >= 1 then
		timepassed = timepassed - 1
		Update()
	end
end

function DPSMeter.OnLButtonUpTab()
	local id = WindowGetId(SystemData.ActiveWindow.name)
	DPSMeter.ChangeTab(id)
end

function DPSMeter.ChangeTab(id)
	if id == 1 then
		ButtonSetPressedFlag(Window.."TabsOverallTabButton", true)
		ButtonSetPressedFlag(Window.."TabsAbilityTabButton", false)
		WindowSetShowing(OverallTab,true)
		WindowSetShowing(AbilityTab,false)
		WindowSetShowing(OverviewTab,false)
	elseif id == 2 then
		ButtonSetPressedFlag(Window.."TabsOverallTabButton", false)
		ButtonSetPressedFlag(Window.."TabsAbilityTabButton", true)
		WindowSetShowing(OverallTab,false)
		WindowSetShowing(OverviewTab,false)
		WindowSetShowing(AbilityTab,true)
	end
	Update()
end

function DPSMeter.AbilityChanged(itemID)
	comboboxID = itemID
	Update()
end

function DPSMeter.ToggleOverview()
	WindowSetShowing(AbilityTab, not WindowGetShowing(AbilityTab))
	WindowSetShowing(OverviewTab, not WindowGetShowing(OverviewTab))
	ListRefresh()
end

function DPSMeter.ShowWindow()
	WindowSetShowing(Window,true)
end

function DPSMeter.ListPopulation()
	if DPSMeterWindowDamageOverviewTabList.PopulatorIndices == nil then return end
	
	local row_window_name 
	local data
	
	for k,v in ipairs(DPSMeterWindowDamageOverviewTabList.PopulatorIndices) do
		row_window_name = OverviewTab.."ListRow"..k
		data = DPSMeter.DamageOverview[v]
		LabelSetText(row_window_name.."Name", data.name)
		LabelSetText(row_window_name.."Damage", towstring(string.sub(data.data,string.find(data.data,"%d+.?%d?%d?%d?"))))
	end
end

function DPSMeter.SortChange()
	local id = WindowGetId(SystemData.ActiveWindow.name)
	local name = OverviewTab.."Name"
	local value = OverviewTab.."Value"
	if id == 1 then
		if sortmode == 1 then
			sortmode = 2
			LabelSetTextColor(name, DefaultColor.RED.r,DefaultColor.RED.g,DefaultColor.RED.b)
			LabelSetTextColor(value, DefaultColor.WHITE.r,DefaultColor.WHITE.g,DefaultColor.WHITE.b)
		else
			LabelSetTextColor(name, DefaultColor.GOLD.r,DefaultColor.GOLD.g,DefaultColor.GOLD.b)
			LabelSetTextColor(value, DefaultColor.WHITE.r,DefaultColor.WHITE.g,DefaultColor.WHITE.b)
			sortmode = 1
		end
	else
		if sortmode == 3 then
			sortmode = 4
			LabelSetTextColor(value, DefaultColor.RED.r,DefaultColor.RED.g,DefaultColor.RED.b)
			LabelSetTextColor(name, DefaultColor.WHITE.r,DefaultColor.WHITE.g,DefaultColor.WHITE.b)
		else
			sortmode = 3
			LabelSetTextColor(value, DefaultColor.GOLD.r,DefaultColor.GOLD.g,DefaultColor.GOLD.b)
			LabelSetTextColor(name, DefaultColor.WHITE.r,DefaultColor.WHITE.g,DefaultColor.WHITE.b)
		end
	end
	ListRefresh()
end

function DPSMeter.ValueChanged(sel)
	valuecombobox = sel
	ListRefresh()
end

function DPSMeter.OnScrollbarLButtonUp()

end